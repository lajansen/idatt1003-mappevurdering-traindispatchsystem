# Portfolio project IDATT1003 - 2023
This file uses Mark Down syntax. For more information see [here](https://www.markdownguide.org/basic-syntax/).

STUDENT NAME = "Lars André Roda Jansen"  
STUDENT ID = "111775"

## Project description

Train dispatch system is an application used to manage train departures in a register.
The application is terminal-based and menu-driven by the user.

## Project structure

Source files are found in src/main/java/edu/ntnu/stud/.

These files are meant for running the main application.

JUnit-tests are found in src/test/java/edu/ntnu/stud/.

These files are meant to verify the functionality of the components in the application.

## Link to repository

https://gitlab.stud.idi.ntnu.no/lajansen/idatt1003-mappevurdering-traindispatchsystem

## How to run the project

Run Train dispatch system by running src/main/java/edu/ntnu/stud/TrainDispatchApp.java-file.

## How to run the tests

Run the JUnit-tests by running the files found in src/test/java/edu/ntnu/stud/.

## References

LocalTime-methods were gotten from https://docs.oracle.com/javase/8/docs/api/java/time/LocalTime.html.

Regex-methods in src/main/java/edu/ntnu/stud/TrainDeparture.java::226 were gotten from a classmate.

How to use String.format method found in src/main/java/edu/ntnu/stud/TrainDeparture.java::252
was gotten from a classmate.

Naming methods assertX if it throws and isXUnique was gotten from a classmate.

System.out.print("""" """") in src/main/java/edu/ntnu/stud/UserInterface.java::302 to create linebreaks was
gotten from a classmate.

GitHub CoPilot was used to help create test-data and JUnit tests.