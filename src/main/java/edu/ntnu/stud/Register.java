package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.*;

/**
 * Class for processing and managing a register/list of trainDepartures.
 */
public class Register {
  /**
   * list of all objects of TrainDeparture class.
   */
  private final ArrayList<TrainDeparture> departures;

  /**
   * Constructor for initializing Register-class.
   */
  public Register() {
    departures = new ArrayList<>();
  }

  /**
   * Gets an newDeparture of TrainDeparture-class, validates object is within
   * criteria to the register, and adds object to departures if it passes validation.
   *
   * @param newDeparture object of trainDeparture-class
   *                     that will be validated before added to departures-list
   */
  public void addTrainDeparture(TrainDeparture newDeparture) {
    assertTrainNumberIsUnique(
        newDeparture.getTrainNumber()
    );
    assertLineAndTimeIsUnique(
        newDeparture.getLine(), newDeparture.getFinalDepartureTime()
    );
    assertTrackAndTimeIsUnique(
        newDeparture.getTrack(), newDeparture.getFinalDepartureTime()
    );

    departures.add(newDeparture);
  }

  /**
   * Gets departures-list.
   * This method is only meant for testing purposes.
   *
   * @return departures in current Register.
   */
  public ArrayList<TrainDeparture> getDepartures() {
    return departures;
  }

  /**
   * Method that finds specific trainDeparture in departures-list based off given trainNumber.
   *
   * @param trainNumber trainNumber that will be used to identify specific trainDeparture.
   * @return trainDeparture with given trainNumber.
   */
  private TrainDeparture getTrainDepartureByTrainNumber(int trainNumber) {
    return departures.stream()
        .filter(departure -> departure.getTrainNumber() == trainNumber)
        .findFirst()
        .orElse(null);
  }

  /**
   * Method that sorts departures by departureTime, then sorts by track.
   *
   * @return List of departures sorted by time, then track.
   */
  private List<TrainDeparture> getDeparturesSortedByTime() {
    return departures
        .stream()
        .sorted(
            Comparator
                .comparing(TrainDeparture::getDepartureTime)
                .thenComparing(TrainDeparture::getTrack)
        )
        .toList();
  }

  /**
   * Method that returns a stream of TrainDepartures that contain a given destination.
   *
   * @param destination given destination-variable to a trainDeparture.
   * @return List of trainDepartures with given destination-value.
   */
  private List<TrainDeparture> getDeparturesByDestination(String destination) {
    return getDeparturesSortedByTime()
        .stream()
        .filter(trainDeparture -> Objects.equals(trainDeparture.getDestination(), destination))
        .toList();
  }

  /**
   * Removes TrainDepartures from departures-list before given time.
   *
   * @param timeString time that will be used to determine which departures to remove, will be
   *                   parsed to LocalTime.
   */
  public void removeDeparturesBeforeTime(String timeString) {
    Utils.assertCorrectLocalTimeFormat(timeString);
    LocalTime time = LocalTime.parse(timeString);

    departures.removeIf(departure -> departure.getFinalDepartureTime().isBefore(time));
  }

  /**
   * Method that removes specific TrainDeparture from departure by given TrainNumber.
   *
   * @param trainNumber identification for which TrainDeparture to remove.
   * @throws IllegalArgumentException if given trainNumber does not belong to a trainDeparture in
   *        departures.
   */
  public void removeTrainDepartureByTrainNumber(int trainNumber) throws IllegalArgumentException {
    if (isTrainNumberUnique(trainNumber)) {
      throw new IllegalArgumentException(
          "Invalid TrainNumber. TrainDeparture with given TrainNumber does not exist"
      );
    }

    departures.removeIf(departure -> departure.getTrainNumber() == trainNumber);
  }

  /**
   * Sets track to a trainDeparture.
   *
   * @param trainNumber used to identify which trainDeparture.
   * @param track variable to change a trainDepartures track to.
   * @throws IllegalArgumentException if given trainNumber does not belong to a trainDeparture in
   *        departures.
   */
  public void setTrackToTrainDeparture(int trainNumber, int track) throws IllegalArgumentException {
    if (isTrainNumberUnique(trainNumber)) {
      throw new IllegalArgumentException(
          "Invalid TrainNumber. TrainDeparture with given TrainNumber does not exist"
      );
    }

    assertTrackAndTimeIsUnique(
        track, getTrainDepartureByTrainNumber(trainNumber).getFinalDepartureTime()
    );

    departures
        .stream()
        .filter(departure -> departure.getTrainNumber() == trainNumber)
        .forEach(departure -> departure.setTrack(track));
  }

  /**
   * Sets a given delay to a given trainDeparture.
   *
   * @param trainNumber used to identify which trainDeparture
   * @param delay variable to change a trainDepartures delay to
   * @throws IllegalArgumentException if given trainNumber does not belong to a trainDeparture in
   *        departures.
   */
  public void setDelayToTrainDeparture(int trainNumber, String delay)
      throws IllegalArgumentException {
    if (isTrainNumberUnique(trainNumber)) {
      throw new IllegalArgumentException(
          "Invalid TrainNumber. TrainDeparture with given TrainNumber does not exist"
      );
    }

    Utils.assertCorrectLocalTimeFormat(delay);
    assertDelayDoesNotCauseLineOrTrackToCollide(trainNumber, LocalTime.parse(delay));

    departures
        .stream()
        .filter(departure -> departure.getTrainNumber() == trainNumber)
        .forEach(departure -> departure.setDelay(delay));
  }

  /**
   * Validates trainNumber.
   * Checks if trainNumber is within 1-999.
   * Checks if trainNumber already exists in departures-list.
   * Throws an exception if not within criteria.
   *
   * @param trainNumber number delegated to be used as identification for a trainDeparture.
   * @throws IllegalArgumentException if TrainNumber is out of scope or
   *        if TrainNumber is already taken.
   */
  private void assertTrainNumberIsUnique(int trainNumber) throws IllegalArgumentException {
    if (!isTrainNumberUnique(trainNumber)) {
      throw new IllegalArgumentException((
          "Invalid TrainNumber. TrainNumber equals that of an existing TrainDeparture"
      ));
    }
  }

  /**
   * Validates track-variable to a trainDeparture.
   * If track has not been delegated (track == -1), skips method.
   * if track-variable:
   * - matches that of another trainDeparture in departures &&
   * - matches totalDepartureTime of another trainDeparture
   * throws new exception if not within criteria.
   *
   * @param track track that a given TrainDeparture departs from.
   * @param time the time that a given TrainDeparture departs.
   * @throws IllegalArgumentException if track at a given time is occupied by another
   *        TrainDeparture.
   */
  private void assertTrackAndTimeIsUnique(int track, LocalTime time)
      throws IllegalArgumentException {
    if (track == -1) {
      return;
    }

    if (!isFinalDepartureTimeAndTrackUnique(time, track)) {
      throw new IllegalArgumentException(
          "Invalid Track or Time. Track is occupied at current time"
      );
    }
  }

  /**
   * Validates line-variable to a trainDeparture.
   * If line-variable:
   * - matches that of another trainDeparture in departures &&
   * - matches finalDepartureTime of another trainDeparture
   * throws new exception if not within criteria.
   *
   * @param line line from trainDeparture that will be validated.
   * @param time time from trainDeparture of when it departs.
   * @throws IllegalArgumentException if line at a given time is occupied by another trainDeparture.
   */
  private void assertLineAndTimeIsUnique(String line, LocalTime time)
      throws IllegalArgumentException {
    if (!isFinalDepartureTimeAndLineUnique(time, line)) {
      throw new IllegalArgumentException("Invalid Line or Time. Line is occupied at current time");
    }
  }

  /**
   * Checks that adding specified delay to specified trainDeparture doesn't collide with other
   *        trainDepartures in departures.
   *
   * @param trainNumber used do identify the trainDeparture.
   * @param delay time which is to be validated.
   * @throws IllegalArgumentException if adding delay to specified trainDeparture will collide
   *        with a line or track to an existing trainDeparture in departures.
   */
  private void assertDelayDoesNotCauseLineOrTrackToCollide(int trainNumber, LocalTime delay)
      throws IllegalArgumentException {
    TrainDeparture tempTrainDeparture = getTrainDepartureByTrainNumber(trainNumber);
    LocalTime tempLocalTime = Utils.addLocalTimes(tempTrainDeparture.getDepartureTime(), delay);

    assertLineAndTimeIsUnique(
        tempTrainDeparture.getLine(), tempLocalTime
    );

    assertTrackAndTimeIsUnique(
        tempTrainDeparture.getTrack(), tempLocalTime
    );
  }

  /**
   * Checks if any trainDeparture in departures has given time and track.
   *
   * @param time time used to match with other trainDepartures in departures.
   * @param track track used to match with other trainDepartures in departures.
   * @return true if no trainDeparture in departures has given time and track.
   */
  private boolean isFinalDepartureTimeAndTrackUnique(LocalTime time, int track) {
    return departures
        .stream()
        .noneMatch(
            departure ->
                departure.getFinalDepartureTime().equals(time)
                    && departure.getTrack() == track
        );
  }

  /**
   * Checks if any trainDeparture in departures has given time and line.
   *
   * @param time time used to match with other trainDepartures in departures.
   * @param line line used to match with other trainDepartures in departures.
   * @return true if no trainDeparture in departures has given time and line.
   */
  private boolean isFinalDepartureTimeAndLineUnique(LocalTime time, String line) {
    return departures
        .stream()
        .noneMatch(
            departure ->
                departure.getFinalDepartureTime().equals(time)
                    && departure.getLine().equals(line)
        );
  }

  /**
   * Checks if given trainNumber does not exist in departures.
   *
   * @param trainNumber number which is used as unique identification for a trainDeparture.
   * @return true if getTrainDepartureByTrainNumber == null.
   */
  private boolean isTrainNumberUnique(int trainNumber) {
    return getTrainDepartureByTrainNumber(trainNumber) == null;
  }

  /**
   * Checks if departures is empty of trainDepartures.
   *
   * @return true if departures is empty, else false.
   */
  public boolean isDeparturesEmpty() {
    return departures.isEmpty();
  }

  /**
   * Creates and returns a string of TrainDeparture.toString()
   *
   * @return String of trainDeparture.toString()
   */
  public String getTrainDeparturesToString() {
    return createTrainDeparturesToString(
        getDeparturesSortedByTime()
    );
  }

  /**
   * Creates and returns a string of trainDepartures in departures, filtered by destination.
   *
   * @param destination variable used to filter trainDepartures.
   * @return String of trainDeparture.toStrings in departure, with a given destination
   */
  public String getTrainDeparturesToString(String destination) {
    return createTrainDeparturesToString(
        getDeparturesByDestination(destination)
    );
  }

  /**
   * Creates and returns a String of a trainDeparture.toString() by given trainNumber.
   *
   * @param trainNumber variable used to identify the trainDeparture
   *                    which is to be returned as a String.
   * @return trainDeparture.toString() matching given trainNumber.
   */
  public String getTrainDeparturesToString(int trainNumber) {
    if (isTrainNumberUnique(trainNumber)) {
      return "No departures found\n";
    }
    return getTrainDepartureByTrainNumber(trainNumber) + "\n";
  }

  /**
   * Creates a string from a Stream of trainDepartures, and returns said string.
   *
   * @param departures Stream of TrainDepartures used to create a String.
   * @return String of given Stream.
   */
  private String createTrainDeparturesToString(List<TrainDeparture> departures) {
    if (departures.isEmpty()) {
      return "No departures found\n";
    }

    StringBuilder stringBuilder = new StringBuilder();
    departures.forEach(trainDeparture -> stringBuilder.append(trainDeparture).append("\n"));
    return stringBuilder.toString();
  }
}
