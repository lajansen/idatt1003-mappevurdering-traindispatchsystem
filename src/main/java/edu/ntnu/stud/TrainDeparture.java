package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class for representing a train departure.
 */
public class TrainDeparture {
  /**
   * Integer >0 that will be used as identification for the trainDeparture.
   * Can't be changed after initialization.
   */
  private final int trainNumber;

  /**
   * String for which line the trainDeparture will follow.
   * Can't be changed after initialization.
   */
  private final String line;

  /**
   * Integer for which track the trainDeparture will ble located at before departing.
   * TrainDepartures with no track delegated is set to -1 as default.
   * May be delegated after initialization.
   */
  private int track = -1;

  /**
   * LocalTime that describes when the trainDeparture is set to depart.
   * Can't be changed after initialization.
   */
  private final LocalTime departureTime;

  /**
   * LocalTime for delay added to the departure time.
   * Set as 00:00 by default.
   * May be delegated after initialization.
   */
  private LocalTime delay = LocalTime.MIN;

  /**
   * String that describes the destination to the trainDeparture.
   * Can't be changed after initialization.
   */
  private final String destination;

  /**
   * Constructor, meant for initialization in the Register-class.
   * Not for use in Register class through UserInterface.
   */
  public TrainDeparture(int trainNumber, String line, int track, String departureTime, String delay,
                        String destination) {
    this(trainNumber, line, departureTime, destination);

    setTrack(track);
    setDelay(delay);
  }

  /**
   * Constructor, meant for usage in the Register-class through the UserInterface-class.
   */
  public TrainDeparture(int trainNumber, String line, String time, String destination) {
    assertNumberIsGreaterThanZero(trainNumber);
    assertLineContainsValidCharacters(line);
    Utils.assertCorrectLocalTimeFormat(time);
    assertDestinationContainsValidCharacters(destination);

    this.trainNumber = trainNumber;
    this.line = line;
    this.departureTime = LocalTime.parse(time);
    this.destination = destination;
  }

  /**
   * Method to set track int.
   * Provided int will be assertd.
   *
   * @param track track where the trainDeparture will depart from.
   */
  public void setTrack(int track) {
    assertNumberIsGreaterThanZero(track);
    this.track = track;
  }

  /**
   * Method to set delay.
   * Provided String will be assertd.
   *
   * @param delay time which the departure of the TrainDeparture will be delayed.
   */
  public void setDelay(String delay) {
    Utils.assertCorrectLocalTimeFormat(delay);
    this.delay = LocalTime.parse(delay);
  }

  /**
   * Method to fetch trainNumber-variable.
   *
   * @return int.
   */
  public int getTrainNumber() {
    return trainNumber;
  }

  /**
   * Method to fetch line-variable.
   *
   * @return String.
   */
  public String getLine() {
    return line;
  }

  /**
   * Method to fetch track-variable.
   *
   * @return Int.
   */
  public int getTrack() {
    return track;
  }

  /**
   * Method to fetch departureTime-variable.
   *
   * @return LocalTime.
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }

  /**
   * Method to fetch delay-variable.
   *
   * @return LocalTime.
   */
  public LocalTime getDelay() {
    return delay;
  }

  /**
   * Method to get total departure time.
   *
   * @return departureTime plus delay.
   */
  public LocalTime getFinalDepartureTime() {
    return Utils.addLocalTimes(departureTime, delay);
  }

  /**
   * Method to fetch destination-variable.
   *
   * @return String.
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Method that checks if given number is >0.
   * Throws an IllegalArgumentException if number is =<0.
   *
   * @param number number to be assertd.
   * @throws IllegalArgumentException if int is =< 0.
   */
  private void assertNumberIsGreaterThanZero(int number) throws IllegalArgumentException {
    if (number <= 0) {
      throw new IllegalArgumentException(
          "Invalid integer. Integer can not be less than or equal to zero"
      );
    }
  }

  /**
   * Method that checks if given destination does not include any non-allowed characters.
   * Checks if string comprises only letters from a-z, A-Z, æ, Æ, ø, Ø, å, Å,
   * space, or ', and has a length of at least 1.
   *
   * @param destination destination-string to be assertd.
   * @throws IllegalArgumentException if the String contains letters
   *        not included in the norwegian alphabet
   */
  private void assertDestinationContainsValidCharacters(String destination)
      throws IllegalArgumentException {
    if (!doesStringMatchPattern(destination, "^[a-zA-ZæøåÆØÅ ']+$")) {
      throw new IllegalArgumentException(
          "Invalid destination. Can only contain characters of the norwegian alphabet"
          + ", and must be at least 1 character in length"
      );
    }
  }

  /**
   * Method that checks if given destination does not include any non-allowed characters.
   * Checks if string comprises only characters from A-Z, 0-9, and is only 1-3 symbols long.
   *
   * @param line line-string to be asserted.
   * @throws IllegalArgumentException if the String contains letters
   *        not included in the norwegian alphabet
   */
  private void assertLineContainsValidCharacters(String line) throws IllegalArgumentException {
    if (!doesStringMatchPattern(line, "^[A-Z0-9]{1,3}$")) {
      throw new IllegalArgumentException(
          "Invalid line. Can only contain symbols A-Z and 0-9"
              + ", and must be of correct format of 1-3 symbols"
      );
    }
  }

  /**
   * Method that checks if given String does not include any non-allowed characters.
   * The allowed characters are given by pattern parameter.
   * Matching is done by regex.
   *
   * @param string String which is to be compared to given Pattern.
   * @param regexPattern Pattern used to compare String to.
   * @return true if given string matches given pattern.
   */
  private boolean doesStringMatchPattern(String string, String regexPattern) {
    Pattern regex = Pattern.compile(regexPattern);
    Matcher result = regex.matcher(string);
    return result.matches();
  }

  /**
   * Formats method-variables to a String.
   * Will not show track-value if track == -1
   * Will not show delay-value if delay == 00:00
   * Code-idea shown to me by fellow a classmate.
   *
   * @return TrainDeparture variables as a String.
   */
  public String toString() {
    String delay = getDelay().toString();
    String track = String.valueOf(getTrack());

    if (getDelay() == LocalTime.MIN) {
      delay = "";
    }

    if (getTrack() == -1) {
      track = "";
    }

    return String.format(
        "%-20s%-20s%-20s%-20s%-20s%-20s",
        getDepartureTime(),
        getLine(),
        getTrainNumber(),
        getDestination(),
        delay,
        track
        );
  }
}
