package edu.ntnu.stud;

/**
 * Train Dispatch App v1.1.0
 * Made by Lars André Roda Jansen
 */
public class TrainDispatchApp {

  /**
   * Main class for train dispatch app.
   * Initializes and starts an instance of UserInterface class.
   *
   * @param args command line arguments. Not used in this program.
   */
  public static void main(String[] args) {
    UserInterface userInterface = new UserInterface();
    userInterface.init();
    userInterface.start();

  }
}
