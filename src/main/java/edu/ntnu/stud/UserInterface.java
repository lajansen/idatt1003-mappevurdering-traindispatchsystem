package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.Scanner;

/**
 * Class for user interaction with the Register.
 */
public class UserInterface {

  /**
   * LocalTime that will determine the time of day for the user using the application.
   */
  private LocalTime timeOfDay = LocalTime.MIN;

  /**
   * Register that will be used to process trainDepartures.
   */
  private Register register;

  /**
   * Scanner that will get user input.
   */
  private final Scanner scanner = new Scanner(System.in);

  /**
   * Used to Initialize the register.
   */
  public void init() {
    register = new Register();
    //addPreMadeDataToRegister();
  }

  /**
   * Used to start the userInterface part of the application.
   */
  public void start() {
    welcomeMessage();
    userClient();
  }

  /**
   * Adds pre-made data of trainDepartures to register.
   * Only used for testing purposes.
   */
  private void addPreMadeDataToRegister() {
    register.addTrainDeparture(
        new TrainDeparture(1, "F1", 2, "06:00", "00:00", "Trondheim")
    );
    register.addTrainDeparture(
        new TrainDeparture(2, "AA3", 3, "06:45", "00:00", "Gjøvik")
    );
    register.addTrainDeparture(
        new TrainDeparture(3, "L12", 4, "08:00", "00:00", "Hematt")
    );
    register.addTrainDeparture(
        new TrainDeparture(4, "VV9", 5, "12:00", "00:00", "Oslo")
    );
  }

  /**
   * Method that contains the main while method for getting userInputs.
   */
  private void userClient() {
    while (true) {
      try {
        printMenu();
        int userInput = getUserIntInput("Option: ");

        switch (userInput) {
          case 3, 4, 5, 6, 7 -> assertRegisterIsNotEmpty();
        }

        switch (userInput) {
          case 1 -> departureBoard();
          case 2 -> addNewDeparture();
          case 3 -> setTrackToDeparture();
          case 4 -> setDelayToDeparture();
          case 5 -> deleteDeparture();
          case 6 -> getDepartureByTrainNumber();
          case 7 -> getDeparturesByDestination();
          case 8 -> updateTimeOfDay();
          case 9 -> exitApp();
          default -> System.out.println("Not an option, try again\n");
        }

      } catch (Exception e) {
        System.out.println(e.getMessage());
        System.out.println("Invalid input, try again\n");

      }
    }
  }

  /**
   * Prints a table of departures in register.
   */
  private void departureBoard() {
    printDepartures();
  }

  /**
   * Adds a new train departure to register.
   * Gets values from user input.
   */
  private void addNewDeparture() {
    departureBoard();

    while (true) {
      try {
        System.out.println("----Add new departure----");

        int trainNumber = getUserIntInput("Train number (Unique integer): ");
        String destination = getUserStringInput("Destination: ");
        String line = getUserStringInput("Line (1-3 symbols of characters A-Z and/or 0-9): ");
        String time = getUserStringInput("Time (hh:mm): ");

        assertTimeIsNotBeforeTimeOfDay(time);

        register.addTrainDeparture(new TrainDeparture(trainNumber, line, time, destination));

        System.out.println("Departure has been added\n");
        break;

      } catch (Exception e) {
        System.out.println(e.getMessage());
        System.out.println("Invalid input(s), try again\n");

      }
    }
  }

  /**
   * Sets a track to a trainDeparture with a given train number.
   * Gets values from user input.
   */
  private void setTrackToDeparture() {
    departureBoard();

    while (true) {
      try {
        System.out.println("----Set track to departure----");

        int trainNumber = getUserIntInput("Train number: ");
        int track = getUserIntInput("Track: ");

        register.setTrackToTrainDeparture(trainNumber, track);

        System.out.println("Track has been set\n");
        break;

      } catch (Exception e) {
        System.out.println(e.getMessage());
        System.out.println("Invalid input, try again\n");

      }
    }
  }

  /**
   * Sets delay to trainDeparture with given train number.
   * Gets values from user input.
   */
  private void setDelayToDeparture() {
    departureBoard();

    while (true) {
      try {
        System.out.println("----Set delay to departure----");

        int trainNumber = getUserIntInput("Train number: ");
        String delay = getUserStringInput("Delay (hh:mm): ");

        register.setDelayToTrainDeparture(trainNumber, delay);

        System.out.println("Delay has been set\n");
        break;

      } catch (Exception e) {
        System.out.println(e.getMessage());
        System.out.println("Invalid input, try again\n");

      }
    }
  }

  /**
   * Deletes trainDeparture from register with given train number.
   * Gets value from user input.
   */
  private void deleteDeparture() {
    departureBoard();

    while (true) {
      try {
        System.out.println("----Delete departure----");

        int trainNumber = getUserIntInput("Train number: ");

        register.removeTrainDepartureByTrainNumber(trainNumber);

        System.out.println("Departure has been deleted\n");
        break;

      } catch (Exception e) {
        System.out.println(e.getMessage());
        System.out.println("Invalid input, try again\n");

      }
    }
  }

  /**
   * Gets trainDeparture with given train number and prints to the user.
   * Gets value from user input.
   */
  private void getDepartureByTrainNumber() {
    departureBoard();

    while (true) {
      try {
        System.out.println("----Get departure with train number----");

        int trainNumber = getUserIntInput("Train number: ");

        printDepartures(trainNumber);
        break;

      } catch (Exception e) {
        System.out.println(e.getMessage());
        System.out.println("Invalid input, try again\n");

      }
    }
  }

  /**
   * Gets departures with given destination and prints to the user.
   * Gets value from user input.
   */
  private void getDeparturesByDestination() {
    departureBoard();

    while (true) {
      try {
        System.out.println("----Get departures by destination----");

        String destination = getUserStringInput("Destination: ");

        printDepartures(destination);
        break;

      } catch (Exception e) {
        System.out.println(e.getMessage());
        System.out.println("Invalid input, try again\n");

      }
    }
  }

  /**
   * Updates timeOfDay variable to a time after current one.
   * Gets variable from user input.
   */
  private void updateTimeOfDay() {
    departureBoard();

    while (true) {
      try {
        System.out.println("----Update time of day----");

        String time = getUserStringInput("New time of day (hh:mm): ");

        Utils.assertCorrectLocalTimeFormat(time);
        assertTimeIsNotBeforeTimeOfDay(time);

        register.removeDeparturesBeforeTime(time);
        timeOfDay = LocalTime.parse(time);

        System.out.println("Time of day has been updated");
        break;

      } catch (Exception e) {
        System.out.println(e.getMessage());
        System.out.println("Invalid input, try again\n");

      }
    }
  }

  /**
   * Exits train dispatch app.
   */
  private void exitApp() {
    System.out.println("----Exiting train dispatch app----");
    System.exit(0);
  }

  /**
   * Prints the menu of methods the user can use.
   */
  private void printMenu() {
    System.out.println("""
        --------|Train dispatch app|--------
        [1] Departure board
        [2] Add new departure
        [3] Set track to departure
        [4] Set delay to departure
        [5] Delete departure
        [6] Get departure with train number
        [7] Get departures with destination
        [8] Update time of day
        [9] Exit
        """);
  }

  /**
   * Prints a table of the departures in register.
   */
  private void printDepartures() {
    printDepartureBoardHeader();

    if (register.isDeparturesEmpty()) {
      System.out.println("No departures found");
    } else {
      System.out.println(register.getTrainDeparturesToString());
    }
  }

  /**
   * Prints a table of given trainDepartures.
   *
   * @param destination variable used to filter which departures to print.
   */
  private void printDepartures(String destination) {
    printDepartureBoardHeader();

    System.out.println(register.getTrainDeparturesToString(destination));
  }

  /**
   * Print a table of given trainDeparture.
   *
   * @param trainNumber variable used to filter which departure to print.
   */
  private void printDepartures(int trainNumber) {
    printDepartureBoardHeader();

    System.out.println(register.getTrainDeparturesToString(trainNumber));
  }

  /**
   * Prints the header for Departure Board.
   */
  private void printDepartureBoardHeader() {
    System.out.println("----Departure board----");
    System.out.println("Time of day: " + timeOfDay + "\n");
    System.out.printf(
        "%-20s%-20s%-20s%-20s%-20s%-20s%n",
        "Departure time",
        "Line",
        "Train number",
        "Destination",
        "Delay",
        "Track"
    );
  }

  /**
   * Checks if given time is before timeOfDay-variable. Throws if it is so.
   *
   * @param time time that will be compared to timeOfDay-variable
   * @throws IllegalArgumentException if given time is before timeOfDay-variable.
   */
  private void assertTimeIsNotBeforeTimeOfDay(String time) throws IllegalArgumentException {
    LocalTime newTime = LocalTime.parse(time);

    if (newTime.isBefore(timeOfDay)) {
      throw new IllegalArgumentException(
          "Invalid Time. Departure time cannot be before current time of day"
      );
    }
  }

  /**
   * Checks if register is empty of trainDepartures.
   *
   * @throws IllegalArgumentException if register is empty.
   */
  private void assertRegisterIsNotEmpty() throws IllegalArgumentException {
    if (register.isDeparturesEmpty()) {
      throw new IllegalArgumentException(
          "Invalid Option. Register is empty, add new departure first"
      );
    }
  }

  /**
   * Gets user integer input. Catches an exception if input is not valid.
   *
   * @param message message which will be printed to the user.
   * @return int from user input
   */
  private int getUserIntInput(String message) {
    int userInput;

    while (true) {
      try {
        System.out.print(message);

        userInput = Integer.parseInt(scanner.nextLine());

        System.out.println();
        break;

      } catch (Exception e) {
        System.out.println(e.getMessage());
        System.out.println("Invalid integer input. Input is not an integer, try again\n");

      }
    }

    return userInput;
  }

  /**
   * Gets user String input.
   *
   * @param message message which will be printed to the user.
   * @return String from user input.
   */
  private String getUserStringInput(String message) {
    String userInput;

    while (true) {
      try {
        System.out.print(message);

        userInput = scanner.nextLine();

        System.out.println();
        break;

      } catch (Exception e) {
        System.out.println(e.getMessage());
        System.out.println("Invalid String input, try again\n");

      }
    }

    return userInput;
  }

  /**
   * Prints out a welcome-message to the user when starting the program.
   */
  private void welcomeMessage() {
    System.out.println("TRAIN DISPATCH APP - MADE BY LARS ANDRÉ RODA JANSEN\n");
  }
}
