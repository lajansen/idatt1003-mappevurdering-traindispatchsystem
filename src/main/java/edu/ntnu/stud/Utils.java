package edu.ntnu.stud;

import java.time.LocalTime;

/**
 * Class for methods meant to be used by several classes.
 */
public class Utils {

  /**
   * Method that checks if a parameter time is:
   * - of the format hh:mm.
   * - hh and mm are valid integers.
   * - hh and mm are not negative integers.
   * Throws an IllegalArgumentException if time does not fit these criteria.
   * While not necessary, as LocalTime throws exceptions by itself, the error messages i created
   * are more user-friendly.
   *
   * @param time String to be validated, so it can be parsed to LocalTime.
   * @throws IllegalArgumentException if String is of wrong format,
   *        String contains non-valid characters,
   *        or numbers in String are negative
   */
  public static void assertCorrectLocalTimeFormat(String time) throws IllegalArgumentException {
    if (time.length() != 5 || time.charAt(2) != ':') {
      throw new IllegalArgumentException("Invalid time format. Correct format hh:mm");
    }

    String[] timeSplit = time.split(":", 0);

    int hh;
    int mm;

    try {
      hh = Integer.parseInt(timeSplit[0]);
      mm = Integer.parseInt(timeSplit[1]);
    } catch (Exception e) {
      throw new IllegalArgumentException("Invalid time. hh and/or mm is not a number");
    }

    if (hh < 0 || mm < 0) {
      throw new IllegalArgumentException("Invalid time. hh and/or mm can not be negative");
    }
  }

  /**
   * Adds one LocalTime value with another LocalTime value.
   *
   * @param time1 first LocalTime value.
   * @param time2 second LocalTime value.
   * @return first and second LocalTime value added.
   */
  public static LocalTime addLocalTimes(LocalTime time1, LocalTime time2) {
    return time1.plusHours(time2.getHour()).plusMinutes(time2.getMinute());
  }

}
