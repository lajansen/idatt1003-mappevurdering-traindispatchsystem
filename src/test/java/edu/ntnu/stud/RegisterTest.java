package edu.ntnu.stud;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import static org.junit.jupiter.api.Assertions.*;

class RegisterTest {

  private Register register;
  @BeforeEach
  void setUp() {
    register = new Register();

    register.addTrainDeparture(
        new TrainDeparture(
            1, "L2", 3, "12:00", "00:30",
            "Hematt"
        )
    );
    register.addTrainDeparture(
        new TrainDeparture(
            4, "L5", 6, "13:00", "00:30",
            "Gjøvik"
        )
    );
    register.addTrainDeparture(
        new TrainDeparture(
            7, "L8", 9, "14:00", "00:30",
            "Toten"
        )
    );
    register.addTrainDeparture(
        new TrainDeparture(
            10, "L11", 12, "15:00", "00:30",
            "Hematt"
        )
    );
    register.addTrainDeparture(
        new TrainDeparture(
            13, "L14", 15, "16:00", "00:30",
            "Gjøvik"
        )
    );
    register.addTrainDeparture(
        new TrainDeparture(
            16, "L17", 18, "17:00", "00:30",
            "Toten"
        )
    );
    register.addTrainDeparture(
        new TrainDeparture(
            19, "L20", 21, "18:00", "00:30",
            "Hematt"
        )
    );


  }

  @AfterEach
  void tearDown() {
    register = null;
  }

  @Test
  void addTrainDeparture() {
    register.addTrainDeparture(
        new TrainDeparture(
            100, "L21", 22, "19:00", "00:30",
            "Hematt"
        )
    );
    register.addTrainDeparture(
        new TrainDeparture(
            101, "L22", 23, "20:00", "00:30",
            "Gjøvik"
        )
    );

    //Cannot add TrainDeparture with same trainNumber as an existing trainDeparture in register
    assertThrows(IllegalArgumentException.class,
        () -> register.addTrainDeparture(
            new TrainDeparture(
                1, "L2", 3, "12:00", "00:30",
                "Hematt"
            )
        ));
    assertThrows(IllegalArgumentException.class,
        () -> register.addTrainDeparture(
            new TrainDeparture(
                4, "L5", 6, "13:00", "00:30",
                "Gjøvik"
            )
        ));

    //Cannot add TrainDeparture with same finalDepartureTime and track as an existing departure in
    // register
    assertThrows(IllegalArgumentException.class,
        () -> register.addTrainDeparture(
            new TrainDeparture(
                100, "L21", 3, "19:00", "00:30",
                "Hematt"
            )
        ));
    assertThrows(IllegalArgumentException.class,
        () -> register.addTrainDeparture(
            new TrainDeparture(
                101, "L22", 6, "20:00", "00:30",
                "Gjøvik"
            )
        ));

    //Cannot add TrainDeparture with same finalDepartureTime and line as an existing departure in register
    assertThrows(IllegalArgumentException.class,
        () -> register.addTrainDeparture(
            new TrainDeparture(
                100, "L2", 22, "19:00", "00:30",
                "Hematt"
            )
        ));
    assertThrows(IllegalArgumentException.class,
        () -> register.addTrainDeparture(
            new TrainDeparture(
                101, "L5", 23, "20:00", "00:30",
                "Gjøvik"
            )
        ));
  }

  @Test
  void removeDeparturesBeforeTime() {
    register.removeDeparturesBeforeTime("16:00");

    assertTrue(register.getDepartures()
        .stream()
        .noneMatch(
            trainDeparture ->
                trainDeparture
                    .getDepartureTime()
                    .isBefore(LocalTime.parse("14:00")
                    )
        )
    );

    register.removeDeparturesBeforeTime("18:00");

    assertTrue(register.getDepartures()
        .stream()
        .noneMatch(
            trainDeparture ->
                trainDeparture
                    .getDepartureTime()
                    .isBefore(LocalTime.parse("18:00")
                    )
        ));

    //Time must be valid format
    assertThrows(IllegalArgumentException.class,
        () -> register.removeDeparturesBeforeTime("1200")
    );
    assertThrows(IllegalArgumentException.class,
        () -> register.removeDeparturesBeforeTime("aa:bb")
    );
    assertThrows(IllegalArgumentException.class,
        () -> register.removeDeparturesBeforeTime("-1:00")
    );
    assertThrows(IllegalArgumentException.class,
        () -> register.removeDeparturesBeforeTime("00:-1")
    );
  }

  @Test
  void removeTrainDepartureByTrainNumber() {
    register.removeTrainDepartureByTrainNumber(1);
    assertTrue(register.getDepartures().stream().noneMatch(departure -> departure.getTrainNumber() == 1));

    register.removeTrainDepartureByTrainNumber(4);
    assertTrue(register.getDepartures().stream().noneMatch(departure -> departure.getTrainNumber() == 4));

    //TrainNumber must be greater than zero
    assertThrows(IllegalArgumentException.class,
        () -> register.removeTrainDepartureByTrainNumber(0)
    );
    assertThrows(IllegalArgumentException.class,
        () -> register.removeTrainDepartureByTrainNumber(-1)
    );
  }

  @Test
  void setTrackToTrainDeparture() {
    register.setTrackToTrainDeparture(1, 100);
    assertEquals(100, register
        .getDepartures()
        .stream()
        .filter(departure -> departure.getTrainNumber() == 1)
        .findFirst()
        .get()
        .getTrack()
    );

    //getFinalDepartureTime and Track cannot equal another departure in register
    register.addTrainDeparture(
        new TrainDeparture(
            300, "L30", 1000, "12:00", "00:30",
            "Hematt"
        )
    );
    register.addTrainDeparture(
        new TrainDeparture(
            301, "L31", "12:00", "Hematt"
        )
    );
    register.setTrackToTrainDeparture(301, 1000);
    assertThrows(IllegalArgumentException.class,
        () -> register.setTrackToTrainDeparture(300, 1000)
    );

    register.addTrainDeparture(
        new TrainDeparture(
            302, "B20", 1000, "19:00", "00:30", "Hematt"
        )
    );
    register.addTrainDeparture(
        new TrainDeparture(
        303, "B21", "19:00", "Hematt")
    );
    register.setTrackToTrainDeparture(303, 1000);
    assertThrows(IllegalArgumentException.class,
        () -> register.setTrackToTrainDeparture(302, 1000)
    );

    //TrainNumber must exist
    assertThrows(IllegalArgumentException.class,
        () -> register.setTrackToTrainDeparture(100, 100)
    );

    //Track must be greater than zero
    assertThrows(IllegalArgumentException.class,
        () -> register.setTrackToTrainDeparture(1, 0)
    );
    assertThrows(IllegalArgumentException.class,
        () -> register.setTrackToTrainDeparture(1, -1)
    );
  }

  @Test
  void setDelayToTrainDeparture() {
    register.setDelayToTrainDeparture(1, "05:30");
    assertEquals(LocalTime.parse("17:30"), register
        .getDepartures()
        .stream()
        .filter(departure -> departure.getTrainNumber() == 1)
        .findFirst()
        .get()
        .getFinalDepartureTime()
    );

    register.setDelayToTrainDeparture(4, "01:00");
    assertEquals(LocalTime.parse("14:00"), register
        .getDepartures()
        .stream()
        .filter(departure -> departure.getTrainNumber() == 4)
        .findFirst()
        .get()
        .getFinalDepartureTime()
    );

    //TrainNumber must exist
    assertThrows(IllegalArgumentException.class,
        () -> register.setDelayToTrainDeparture(100, "00:30")
    );

    //getFinalDepartureTime and Track cannot equal another departure in register
    register.addTrainDeparture(
        new TrainDeparture(
            300, "L30", 1000, "12:00", "00:30",
            "Hematt"
        )
    );
    register.addTrainDeparture(
        new TrainDeparture(
            301, "L31", "12:00", "Hematt"
        )
    );
    register.setTrackToTrainDeparture(301, 1000);
    assertThrows(IllegalArgumentException.class,
        () -> register.setDelayToTrainDeparture(301, "00:30")
    );

    //getFinalDepartureTime and Line cannot equal another departure in register
    register.addTrainDeparture(
        new TrainDeparture(
            302, "B20", 1000, "19:00", "00:30",
            "Hematt"
        )
    );
    register.addTrainDeparture(
        new TrainDeparture(
            303, "B21", "19:00", "Hematt"
        )
    );
    assertThrows(IllegalArgumentException.class,
        () -> register.setDelayToTrainDeparture(305, "01:00"));

    //Delay must be valid format
    assertThrows(IllegalArgumentException.class,
        () -> register.setDelayToTrainDeparture(1, "0030")
    );
    assertThrows(IllegalArgumentException.class,
        () -> register.setDelayToTrainDeparture(1, "aa:bb")
    );
    assertThrows(IllegalArgumentException.class,
        () -> register.setDelayToTrainDeparture(1, "-1:00")
    );
    assertThrows(IllegalArgumentException.class,
        () -> register.setDelayToTrainDeparture(1, "00:-1")
    );

  }

  @Test
  void departuresIsEmpty() {
    Register testRegister = new Register();
    assertTrue(testRegister.isDeparturesEmpty());

    testRegister.addTrainDeparture(
        new TrainDeparture(
            1, "L2", 3, "12:00", "00:30",
            "Hematt"
        )
    );
    assertFalse(testRegister.isDeparturesEmpty());
  }
}