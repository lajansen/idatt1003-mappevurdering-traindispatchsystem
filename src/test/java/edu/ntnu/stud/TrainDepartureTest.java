package edu.ntnu.stud;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;

class TrainDepartureTest {
  private TrainDeparture testTrainDeparture;
  @BeforeEach
  void setUp() {
    testTrainDeparture = new TrainDeparture(
        1, "A10", "12:00", "Hematt"
    );

  }

  @Test
  void constructor() {
    assertDoesNotThrow(() -> new TrainDeparture(
        1, "F1F", 1, "12:00", "00:30",
        "abcdefghijklmnopqrstuvwxyzæøåABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅ '"));
    assertDoesNotThrow(() -> new TrainDeparture(2, "A", "12:00",
        "abcdefghijklmnopqrstuvwxyzæøåABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅ '"));

    //TrainNumber and Track cannot equal or be less than zero

    //Track
    assertThrows(IllegalArgumentException.class,
        () -> new TrainDeparture(
            1, "F1", 0, "12:00", "00:00", "Hematt"
        ));
    assertThrows(IllegalArgumentException.class,
        () -> new TrainDeparture(
            1, "F1", -1, "12:00", "00:00", "Hematt"
        ));

    //departureTime or delay cannot be invalid format
    //departureTime
    assertThrows(IllegalArgumentException.class,
        () -> new TrainDeparture(
          2, "F1", 1, "1200", "00:30", "Hematt"
        ));
    assertThrows(IllegalArgumentException.class,
        () -> new TrainDeparture(
            1, "F1", 1, "aa:bb", "00:30", "Hematt"
        ));
    assertThrows(IllegalArgumentException.class,
        () -> new TrainDeparture(
            1, "F1", 1, "-1:00", "00:30", "Hematt"
        ));
    assertThrows(IllegalArgumentException.class,
        () -> new TrainDeparture(
            1, "F1", 1, "00:-1", "00:30", "Hematt"
        ));

    //delay
    assertThrows(IllegalArgumentException.class,
        () -> new TrainDeparture(
            1, "F1", 1, "12:00", "0030", "Hematt"
        ));
    assertThrows(IllegalArgumentException.class,
        () -> new TrainDeparture(
            1, "F1", 1, "12:00", "aa:bb", "Hematt"
        ));
    assertThrows(IllegalArgumentException.class,
        () -> new TrainDeparture(
            1, "F1", 1, "12:00", "-1:00", "Hematt"
        ));
    assertThrows(IllegalArgumentException.class,
        () -> new TrainDeparture(
            1, "F1", 1, "12:00", "00:-1", "Hematt"
        ));

    //Destination can only contain allowed characters and must be 1 character or longer
    assertThrows(IllegalArgumentException.class,
        () -> new TrainDeparture(
            1, "F1", 1, "12:00", "00:30", "1"
        ));
    assertThrows(IllegalArgumentException.class,
        () -> new TrainDeparture(
            1, "F1", 1, "12:00", "00:30", ""
        ));

    //Line can only contain allowed characters and must be 1-3 characters
    assertThrows(IllegalArgumentException.class,
        () -> new TrainDeparture(
            1, "", 1, "12:00", "00:30", "Hematt"
        ));
    assertThrows(IllegalArgumentException.class,
        () -> new TrainDeparture(
            1, "f", 1, "12:00", "00:30", "Hematt"
        ));
    assertThrows(IllegalArgumentException.class,
        () -> new TrainDeparture(
            1, "F120", 1, "12:00", "00:30", "Hematt"
        ));


  }

  @Test
  void setTrack() {
    testTrainDeparture.setTrack(1);

    //Track cannot equal or be less than zero
    assertThrows(IllegalArgumentException.class, () -> testTrainDeparture.setTrack(0));
    assertThrows(IllegalArgumentException.class, () -> testTrainDeparture.setTrack(-1));
  }

  @Test
  void setDelay() {
    testTrainDeparture.setDelay("00:30");

    //Delay cannot be invalid format
    assertThrows(IllegalArgumentException.class, () -> testTrainDeparture.setDelay("0030"));
    assertThrows(IllegalArgumentException.class, () -> testTrainDeparture.setDelay("aa:bb"));
    assertThrows(IllegalArgumentException.class, () -> testTrainDeparture.setDelay("-1:30"));
    assertThrows(IllegalArgumentException.class, () -> testTrainDeparture.setDelay("00:-1"));
  }

  @Test
  void getTrainNumber() {
    var test1 = new TrainDeparture(1, "F1", "12:00", "Hematt");
    var test2 = new TrainDeparture(2, "F2", "12:00", "Hematt");
    var test3 = new TrainDeparture(3, "F3", "12:00", "Hematt");

    assertEquals(1, test1.getTrainNumber());
    assertEquals(2, test2.getTrainNumber());
    assertEquals(3, test3.getTrainNumber());
  }

  @Test
  void getFinalDepartureTime() {
    assertEquals(LocalTime.parse("12:00"), testTrainDeparture.getFinalDepartureTime());

    testTrainDeparture.setDelay("00:00");
    assertEquals(LocalTime.parse("12:00"), testTrainDeparture.getFinalDepartureTime());

    testTrainDeparture.setDelay("00:30");
    assertEquals(LocalTime.parse("12:30"), testTrainDeparture.getFinalDepartureTime());
  }

  @Test
  void testToString() {
    assertEquals(
        "12:00               " + "A10                 " + "1                   " +
            "Hematt              " + "                    " + "                    ",
        testTrainDeparture.toString()
    );

    var test = new TrainDeparture(
        2, "F1", 4, "13:00", "00:30", "Gjøvik"
    );

    assertEquals(
        "13:00               " + "F1                  " + "2                   " +
            "Gjøvik              " + "00:30               " + "4                   ",
        test.toString()
    );
  }
}