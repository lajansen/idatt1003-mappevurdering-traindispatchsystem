package edu.ntnu.stud;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;

class UtilsTest {

  @Test
  void validateTimeFormat() {
    Utils.assertCorrectLocalTimeFormat("12:00");

    //Time cannot be invalid format
    assertThrows(IllegalArgumentException.class,
        () -> Utils.assertCorrectLocalTimeFormat("1200"));
    assertThrows(IllegalArgumentException.class,
        () -> Utils.assertCorrectLocalTimeFormat("aa:bb"));
    assertThrows(IllegalArgumentException.class,
        () -> Utils.assertCorrectLocalTimeFormat("-1:00"));
    assertThrows(IllegalArgumentException.class,
        () -> Utils.assertCorrectLocalTimeFormat("00:-1"));
  }

  @Test
  void addLocalTimes() {
    assertEquals(LocalTime.parse("12:00"), Utils.addLocalTimes(LocalTime.parse("11:30"), LocalTime.parse("00:30")));
    assertEquals(LocalTime.parse("12:00"), Utils.addLocalTimes(LocalTime.parse("11:00"), LocalTime.parse("01:00")));
  }
}